---
title: Conditions
linkTitle: Conditions
menu:
  main:
    identifier: Conditions
weight: -250
slug: Conditions
---

Coefficient :  700

La mutuelle :

Je ne souhaite pas bénéficier de la mutuelle d'entreprise. Il est nécessaire d'en parler à votre comptable et de lui transmettre le papier d'exemption de mutuelle que je vous fournirai.