---
title: Accueil
linkTitle: Accueil
menu:
  main:
    identifier: Accueil
weight: -250
slug: Accueil
---

**Vous êtes titulaire d'officine et recherchez un pharmacien pour remplacer**

**un ou une membre de votre équipe ou encore vous-même ?**


**Vous êtes au bon endroit !**


Après l'obtention de mon diplôme de pharmacien en 2012, puis ma thèse en 2013, j'ai acquis une officine en zone rurale en 2014.

Après un transfert réussi l'année suivante et une progression de + de 40% en 8 ans, je l’ai transmise en 2022.


Durant mon exercice je me suis beaucoup investi à la fois syndicalement en devenant président du syndicat départemental en 2016 et en interpro, en créant et en présidant l'Equipe de Soins Primaires (ESP) de la commune et la Communauté de Professionnelle Territoriale de Santé du territoire (CPTS).


Dorénavant j'ai fait le choix de me déplacer dans **toute la France** pour des remplacements courts, afin de diversifier les expériences professionnelles et transmettre mes connaissances.

Bonne navigation et à bientôt !		