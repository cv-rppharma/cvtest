---
title: Agenda
linkTitle: Agenda
menu:
  main:
    identifier: Agenda
weight: -250
slug: Agenda
---

Cet agenda donne un aperçu de mes disponibilités à un moment donné. N'hésitez pas à le consulter régulièrement car des imprévus peuvent faire varier mes disponibilités !

Dernière MAJ : 02/01/2023

2023 : INDISPONIBLE

2024 : Consultable à partir de fin 2023.